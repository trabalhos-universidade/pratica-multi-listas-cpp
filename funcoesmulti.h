#include "funcoes.h"

class multi
{
    private:
        list<list<int> > ml;
        
    public:
        list<list<int> >::iterator auxml;  //colocar em todas que precisam
        list<int>::iterator auxl;   //colocar em todas que precisam
        multi();
        ~multi();
        int getsize();
        void getsizesublista();
        int getvalor();
        bool multilistavazia();
        bool listavazia(int sublista);
        void insereinicio(int sublista,int valor);
        void inserefinal(int sublista,int valor);
        void removeinicio(int sublista);
        void removefinal(int sublista);
        int getvalorbegin(int sublista);
        int getvalorend(int sublista);
        void consulta();
        void consultaesp(int valor);
        void consultaprimeirosublista(int sublista);
        void consultaultimosublista(int sublista);
        void invertemultilista();
        void invertesublista(int sublista);
        void alteraelemento(int valor);
        void removesublista(int sublista);
        void removemultilista();
        void consultaprimeiro();
        void consultaultimo();
        void trocaposicao(int valor, int valor2);
        void localizamultilista(int valor);
        void removerepetidos();
        void ordena();
};
