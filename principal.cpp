#include "funcoesmulti.h"

int menu();

main() {

    int i,op,sub,valor,valor2;
    multi *ml = new multi;
    list<list<int> >::iterator auxml;
    list<int>::iterator auxl;
    do {
        op=menu();
        switch(op) {
            case 1: ml = new multi;
                    cout<<"\nMultilista recriada com sucesso\n";
                    system("pause");
            break;
            
            case 2: cout<<endl<<"Informe a sublista a ser utilizada: ";
                    cin>>sub;
                    if (sub < ml->getsize()+1)
                    {
                        cout<<endl<<"Informe o valor: ";
                        cin>>valor;
                        ml->insereinicio(sub,valor);
                        cout<<endl<<"Valor inserido com sucesso";
                    }
                    else
                        cout<<endl<<"Sublista incorreta";
            break;
            
            case 3: cout<<endl<<"Informe a sublista a ser utilizada: ";
                    cin>>sub;
                    if (sub < ml->getsize()+1)
                    {
                        cout<<endl<<"Informe o valor: ";
                        cin>>valor;
                        ml->inserefinal(sub,valor);
                        cout<<endl<<"Valor inserido com sucesso";
                    }
                    else
                        cout<<endl<<"Sublista incorreta";
            break;
            
            case 4: cout<<endl<<"Informe a sublista a ser utilizada: ";
                    cin>>sub;
                    if (sub < ml->getsize()+1)
                    {
                        if (!ml->listavazia(sub))
                        {
                            cout<<"Valor removido da sublista "<<sub<<": "<<ml->getvalorbegin(sub)<<"\n";
                            ml->removeinicio(sub);
                        }
                        else
                            cout<<endl<<"Sublista vazia. Impossivel remover\n";
                    }
                    else
                        cout<<endl<<"Sublista incorreta\n";
                    system("pause");
            break;
            
            case 5: cout<<endl<<"Informe a sublista a ser utilizada: ";
                    cin>>sub;
                    if (sub < ml->getsize()+1)
                    {
                        if (!ml->listavazia(sub))
                        {
                            cout<<"Valor removido da sublista "<<sub<<": "<<ml->getvalorend(sub)<<"\n";
                            ml->removefinal(sub);
                        }
                        else
                            cout<<endl<<"Sublista vazia. Impossivel remover\n";
                    }
                    else
                        cout<<endl<<"Sublista incorreta\n";
                    system("pause");
            break;
            
            case 6: if (!ml->multilistavazia())
                    {
                        cout<<endl<<"Informe o elemento a ser removido: ";
                        cin>>valor;
                        ml->consultaesp(valor);
                        cout<<"\nElemento removido";
                    }
                    else
                        cout<<endl<<"Multilista vazia\n";
                    system("pause");
            break;
            
            case 7: if (!ml->multilistavazia())
                    {
                        cout<<endl<<"Informe a sublista: ";
                        cin>>sub;
                        if (sub < ml->getsize()+1)
                        {
                            ml->removesublista(sub);
                            cout<<endl<<"Sublista "<<sub<<" removida\n";
                        }
                        else
                            cout<<endl<<"Sublista incorreta\n";
                    }
                    else
                        cout<<endl<<"Multilista vazia\n";
                    system("pause");
            break;
            
            case 8: if (!ml->multilistavazia())
                    {
                        delete ml;
                        cout<<endl<<"Multilista removida\n";
                    }
                    else
                        cout<<endl<<"Multilista inexistente\n";
                    system("pause");
            break;
            
            case 9: if (!ml->multilistavazia())
                    {
                        cout<<endl<<"Elementos da multilista"<<endl<<endl;
                        ml->consulta();
                        cout<<endl;
                    }
                    else
                        cout<<endl<<"Multilista vazia\n";
                    system("pause");
            break;
            
            case 10: if (!ml->multilistavazia())
                     {
                         ml->consultaprimeiro();
                         cout<<endl;
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 11: if (!ml->multilistavazia())
                     {
                         ml->consultaultimo();
                         cout<<endl;
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 12: if (!ml->multilistavazia())
                     {
                         cout<<endl<<"Informe a sublista: ";
                         cin>>sub;
                         if (sub < ml->getsize()+1)
                             ml->consultaprimeirosublista(sub);
                         else
                             cout<<endl<<"Sublista inexistente\n";
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 13: if (!ml->multilistavazia())
                     {
                         cout<<endl<<"Informe a sublista: ";
                         cin>>sub;
                         if (sub < ml->getsize()+1)
                            ml->consultaultimosublista(sub);
                         else
                             cout<<endl<<"Sublista inexistente\n";
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;

            case 14: if (!ml->multilistavazia())
                         cout<<endl<<"Numero de elementos da multilista: "<<ml->getsize()<<"\n";
                     else
                         cout<<endl<<"Numero de elementos da multilista: 0\n";
                     system("pause");
            break;

            case 15: if (!ml->multilistavazia())
                     {
                         cout<<endl<<"Numero de elementos das sublistas\n";
                         ml->getsizesublista();
                     }
                     else
                         cout<<endl<<"Multilista vazia. Nao ha elementos nas sublistas\n";
                     system("pause");
            break;
            
            case 16: if (!ml->multilistavazia())
                     {
                         ml->invertemultilista();
                         cout<<"\nA multilista foi invertida\n";
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 17: if (!ml->multilistavazia())
                     {
                         cout<<"\nInforme a sublista: ";
                         cin>>sub;
                         if (sub < ml->getsize()+1)
                         {
                             ml->invertesublista(sub);
                             cout<<"\nA sublista "<<sub<<" foi invertida\n";
                         }
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 18: if (!ml->multilistavazia())
                     {
                         cout<<"\nInforme o primeiro valor: ";
                         cin>>valor;
                         cout<<"Informe o segundo valor: ";
                         cin>>valor2;
                         ml->trocaposicao(valor,valor2);
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 19: if (!ml->multilistavazia())
                     {
                         cout<<"\nInforme o elemento: ";
                         cin>>valor;
                         ml->localizamultilista(valor);
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 20: if (!ml->multilistavazia())
                     {
                         ml->removerepetidos();
                         cout<<"\nOs elementos repetidos de cada sublista foram removidos\n";
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;
            
            case 21: if (!ml->multilistavazia())
                     {
                         ml->ordena();
                         cout<<"\nMultilista ordenada\n";
                     }
                     else
                         cout<<endl<<"Multilista vazia\n";
                     system("pause");
            break;            
            
            case 22: if (!ml->multilistavazia())
                     {
                         cout<<endl<<"Informe o elemento a ser alterado: ";
                         cin>>valor;
                         ml->alteraelemento(valor);
                     }
                     else
                         cout<<"\nMultilista vazia\n";
                     system("pause");
            break;
        }
    }while (op!=0);
}

int menu() {
    int op=0;
    system("cls");
    cout<<"\n\nPratica - STL Multilist";
    cout<<"\n\n\n 1- Reriar (5 multilistas)";
    cout<<"\n 2- Incluir no inicio (informar em qual sublista)";
    cout<<"\n 3- Incluir no final (informar em qual sublista)";
    cout<<"\n 4- Remover pelo inicio (informar em qual sublista)" ;
    cout<<"\n 5- Remover pelo final (informar em qual sublista)";
    cout<<"\n 6- Remover especifico (elemento)";
    cout<<"\n 7- Remover sublista (informar em qual sublista)";
    cout<<"\n 8- Remover multilista";
    cout<<"\n 9- Consultar multilista";
    cout<<"\n 10- Consultar primeiro da multilista";
    cout<<"\n 11- Consultar ultimo da multilista";
    cout<<"\n 12- Consultar primeiro da sublista (informar qual sublista)";
    cout<<"\n 13- Consultar ultimo da sublista (informar qual sublista)";
    cout<<"\n 14- Numero de elementos da multilista";
    cout<<"\n 15- Numero de elementos de cada sublista";
    cout<<"\n 16- Inverter ordem da multilista";
    cout<<"\n 17- Inverter ordem da sublista (informar qual)";
    cout<<"\n 18- Trocar dois elementos m e n de posicao na multilista";
    cout<<"\n 19- Localizar um elemento na multilista";
    cout<<"\n 20- Remover elementos repetidos (de cada sublista)";
    cout<<"\n 21- Ordenar elementos (de cada sublista)";
    cout<<"\n 22- Alterar um elementos";
    cout<<"\n\n 0- Sair";
    cout<<"\n\nDigite a opcao: ";
    cin>>op;
    return op;
}
