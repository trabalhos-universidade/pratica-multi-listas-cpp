#include "funcoesmulti.h"

multi::multi()
{
    list<int> l;
    ml.clear();
    for (int i=1;i<6;i++)
        ml.push_back(l);
}

multi::~multi()
{
    ml.clear();
}

void multi::insereinicio(int sublista, int valor)
{
    auxml = ml.begin();
    for (int i=1;i<sublista;i++)
        auxml++;
    auxml->push_front(valor);
}

void multi::inserefinal(int sublista, int valor)
{
    auxml = ml.begin();
    for (int i=1;i<sublista;i++)
        auxml++;
    auxml->push_back(valor);
}

void multi::removeinicio(int sublista)
{
    auxml = ml.begin();
    for(int i=1;i<sublista;i++)
        auxml++;
    auxml->pop_front();
}

void multi::removefinal(int sublista)
{
    auxml = ml.begin();
    for(int i=1;i<sublista;i++)
        auxml++;
    auxml->pop_back();
}

int multi::getsize()
{
    return ml.size();
}

void multi::getsizesublista()
{
    auxml = ml.begin();
    for(int i=1;i<getsize();i++)
    {
        cout<<"\nSublista "<<i<<": "<<auxml->size()<<"\n";
        auxml++;
    }
    cout<<"\nSublista "<<getsize()<<": "<<auxml->size()<<"\n";
}

void multi::invertemultilista()
{
    ml.reverse();
}

void multi::invertesublista(int sublista)
{
    auxml = ml.begin();
    for (int i=1;i<sublista;i++)
    {
        auxml++;
    }
    auxml->reverse();
}

/*list<int> *multi::getbegin()
{
    return ml.begin();
}

list<int> *multi::getend()
{
    return ml.end();
}*/

int multi::getvalorbegin(int sublista)
{
    auxml = ml.begin();
    for(int i=1;i<sublista;i++)
        auxml++;
    return auxml->front();
}

int multi::getvalorend(int sublista)
{
    auxml = ml.begin();
    for(int i=1;i<sublista;i++)
        auxml++;
    return auxml->back();
}

int multi::getvalor()
{
    return *auxl;
}

bool multi::multilistavazia()
{
    return ml.empty();
}

bool multi::listavazia(int sublista)
{
    auxml = ml.begin();
    for(int i=1;i<sublista;i++)
        auxml++;
    return auxml->empty();
}

void multi::consulta()
{
    for(auxml=ml.begin();auxml!=ml.end();auxml++)
        if(!auxml->empty())
        {
            cout<<endl;
            for(auxl=auxml->begin();auxl!=auxml->end();auxl++)
                cout<<getvalor()<<"     ";
            cout<<endl;
        }
        else
            cout<<"\nSublista vazia\n";
}

void multi::consultaesp(int valor)
{
    auxml = ml.begin();
    while(auxml != ml.end())
    {
        auxl = find(auxml->begin(),auxml->end(),valor);
        if (auxl != auxml->end())
            break;
        auxml++;
    }
    if (auxml != ml.end())
        auxml->erase(auxl);
    else
        cout<<"\nElemento nao encontrado";
}

void multi::consultaprimeirosublista(int sublista)
{
    auxml = ml.begin();
    for (int i=1;i<sublista;i++)
        auxml++;
    if (!listavazia(sublista))
        cout<<"\nPrimeiro elemento da sublista: "<<auxml->front();
    else
        cout<<"\nSublista vazia";
}

void multi::consultaultimosublista(int sublista)
{
    auxml = ml.begin();
    for (int i=1;i<sublista;i++)
        auxml++;
    if (!listavazia(sublista))
        cout<<"\nPrimeiro elemento da sublista: "<<auxml->back();
    else
        cout<<"\nSublista vazia";
}

void multi::alteraelemento(int valor)
{
    auxml = ml.begin();
    while (auxml != ml.end())
    {
        auxl = find(auxml->begin(),auxml->end(),valor);
        if (auxl != auxml->end())
            break;
        auxml++;
    }
    if (auxml != ml.end())
    {
        cout<<endl<<"Informe o novo valor: ";
        cin>>valor;
        *auxl = valor;
    }
    else
        cout<<endl<<"Elemento nao encontrado\n";
}

void multi::removesublista(int sublista)
{
    auxml = ml.begin();
    for (int i=1;i<sublista;i++)
        auxml++;
    ml.erase(auxml);
}

void multi::consultaprimeiro()
{
    auxml = ml.begin();
    if (!auxml->empty())
    {
        for(auxl=auxml->begin();auxl!=auxml->end();auxl++)
            cout<<getvalor()<<"     ";
    }
    else
        cout<<"\nSublista vazia\n";
}

void multi::consultaultimo()
{
    auxml = ml.begin();
    for (int i=1;i<getsize();i++)
        auxml++;
    if (!auxml->empty())
    {
        for (auxl=auxml->begin();auxl!=auxml->end();auxl++)
            cout<<getvalor()<<"     ";
    }
    else
        cout<<"\nSublista vazia\n";
}

// nao esta funcionando corretamente (verificar iterator)

void multi::trocaposicao(int valor, int valor2)
{
    list<int>::iterator aux, aux2, aux3, end1, end2;
    auxml = ml.begin();
    aux = auxml->begin();
    aux2 = auxml->begin();
    aux3 = auxml->begin();
    end1 = auxml->begin();
    end2 = auxml->begin();
    while(auxml != ml.end())
    {
        auxl = find(auxml->begin(),auxml->end(),valor);
        if (auxl != auxml->end())
        {
            aux = auxl;
            end1 = auxml->end();
        }    
        else
        {
            auxl = find(auxml->begin(),auxml->end(),valor2);
            if (auxl != auxml->end())
            {
                aux2 = auxl;
                end2 = auxml->end();
            }    
        }
        if ((aux != end1) && (aux2 != end2))
        {
            //system("pause");
            *aux3 = *aux;
            *aux = *aux2;
            *aux2 = *aux3;
            break;
        }
        auxml++;
    }
    if (auxml != ml.end())
        cout<<"\nElementos trocados com sucesso\n";
    else
        cout<<"\nElemento nao encontrado\n";
}

void multi::localizamultilista(int valor)
{
    int sublista = 1;
    auxml = ml.begin();
    while(auxml != ml.end())
    {
        auxl = find(auxml->begin(),auxml->end(),valor);
        if (auxl != auxml->end())
            break;
        auxml++;
        sublista++;
    }
    if (auxml != ml.end())
        cout<<"\nElementos encontrado na sublista "<<sublista<<"\n";
    else
        cout<<"\nElemento nao encontrado\n";
}

void multi::removerepetidos()
{
    ordena();
    auxml = ml.begin();
    while(auxml != ml.end())
    {
        auxml->unique();
        auxml++;
    }
}

void multi::ordena()
{
    auxml = ml.begin();
    while(auxml != ml.end())
    {
        auxml->sort();
        auxml++;
    }   
}
